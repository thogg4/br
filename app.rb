require 'open-uri'

require 'bundler'
Bundler.require

set :bind, '0.0.0.0'
set :database, { adapter: 'sqlite3', database: 'br.sqlite3' }

get '/stats' do
  content_type :json

  hash = {}

  Stat.kinds.each do |kind|
    if params['players']
      hash[kind] = Stat.joins(:player).where(kind: kind).where("players.slug IN (?)", params['players'].split(',')).map { |stat| stat.for_json }
    else
      hash[kind] = Stat.where(kind: kind).map { |stat| stat.for_json }
    end
  end

  hash.to_json
end

post '/football' do
  file = open('https://gist.githubusercontent.com/thogg4/5071b5a60dea6f98917ffb2ae3b0e294/raw/87db30a17058ebcc3843db7dd0dec5f4e59d7f79/gistfile1.json')
  json = JSON.parse(file.read)
  import(json)
  'imported data'
end

def import(data)
  period = StatPeriod.where(season: data['seasonID'], period: data['week']).first_or_create
  
  kinds = ['rushing', 'passing', 'receiving', 'kicking']
  kinds.each do |kind|
    data[kind].each do |stats|
      player = Player.where({
        name: stats.delete('name'),
        position: stats.delete('position'),
        external_id: stats.delete('player_id')
      }).first_or_create

      new_stats = Stat.where(external_id: stats.delete('entry_id')).first_or_initialize

      stats.each { |key, value| new_stats.send("#{key}=", value) }
      
      new_stats.kind = kind
      new_stats.player = player
      new_stats.stat_period = period

      new_stats.save!
    end
  end
end

class Player < ActiveRecord::Base
  validates :name, presence: true

  has_many :stats

  before_validation :set_slug
  def set_slug
    self.slug = self.name.downcase.gsub(/\s/, '-')
  end

end

class StatPeriod < ActiveRecord::Base
  validates :season, presence: true
  validates :period, presence: true

  has_many :stats
end

class Stat < ActiveRecord::Base
  validates :kind, presence: true

  belongs_to :player
  belongs_to :stat_period

  def stat_attributes
    attrs = ['yds', 'att', 'tds', 'fum', 'cmp', 'int', 'rec', 'fld_goals_made', 'fld_goals_att', 'extra_pt_made', 'extra_pt_att']
    self.attributes.select { |key, value| attrs.include?(key) }
  end

  def self.kinds
    Stat.select(:kind).distinct.map {|stat| stat.kind }
  end

  def for_json
    hash = {}
    hash['player_id'] = self.player.external_id
    hash['entry_id'] = self.external_id
    hash['name'] = self.player.name
    hash['position'] = self.player.position

    self.stat_attributes.each { |key, value| hash[key] = value if value != nil }

    hash
  end

end
