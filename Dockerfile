FROM ruby:2.3-alpine

RUN apk add --update alpine-sdk sqlite-dev

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile /app/
COPY Gemfile.lock /app/

RUN bundle install

COPY . /app

EXPOSE 4567

CMD ["ruby", "/app/app.rb"]
