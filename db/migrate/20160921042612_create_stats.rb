class CreateStats < ActiveRecord::Migration
  def change
    create_table :stats do |t|
      t.string :kind
      t.string :yds
      t.string :att
      t.string :tds
      t.string :fum
      t.string :cmp
      t.string :int
      t.string :rec
      t.string :fld_goals_made
      t.string :fld_goals_att
      t.string :extra_pt_made
      t.string :extra_pt_att
      t.string :external_id

      t.integer :player_id
      t.integer :stat_period_id

      t.timestamps
    end
  end
end
