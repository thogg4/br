class CreateStatPeriods < ActiveRecord::Migration
  def change
    create_table :stat_periods do |t|
      t.string :season
      t.string :period

      t.timestamps
    end
  end
end
