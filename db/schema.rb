# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160921042612) do

  create_table "players", force: :cascade do |t|
    t.string   "name"
    t.string   "position"
    t.string   "slug"
    t.string   "external_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stat_periods", force: :cascade do |t|
    t.string   "season"
    t.string   "period"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stats", force: :cascade do |t|
    t.string   "kind"
    t.string   "yds"
    t.string   "att"
    t.string   "tds"
    t.string   "fum"
    t.string   "cmp"
    t.string   "int"
    t.string   "rec"
    t.string   "fld_goals_made"
    t.string   "fld_goals_att"
    t.string   "extra_pt_made"
    t.string   "extra_pt_att"
    t.string   "external_id"
    t.integer  "player_id"
    t.integer  "stat_period_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
