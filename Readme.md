Some notes
===

* Football data could be posted to the app at `/football`  (maybe with something like a webhook) right now the data url is hardcoded
* `/stats` returns all stats and `/stats?players=player-slug` returns player specific data
* You can use docker - `docker build -t br .` then `docker run --rm -p 4567:4567 br`
* Or on your machine - `bundle install` then `ruby app.rb`

working urls:

* `localhost:4567/stats?players=aaron-rodgers`
* `localhost:4567/stats?players=aaron-rodgers,ahmad-bradshaw`
